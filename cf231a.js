// https://codeforces.com/problemset/problem/231/A

function main(stdin) {
  stdin.shift()
  process.stdout.write(
    stdin.filter(p => (p.match(/1/g) || []).length >= 2).length + '')
}

// Handle standard input
process.stdin.resume()
process.stdin.setEncoding('utf-8')

let inputString = ''

process.stdin.on('data', (inputStdin) => inputString += inputStdin)
process.stdin.on('end', () =>
  main(inputString.trim().split('\n').map(s => s.trim())))

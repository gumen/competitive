// https://codeforces.com/problemset/problem/339/A

function main(stdin) {
  process.stdout.write(stdin[0].split('+').sort().join('+') + '')
}

// Handle standard input
process.stdin.resume()
process.stdin.setEncoding('utf-8')

let inputString = ''

process.stdin.on('data', (inputStdin) => inputString += inputStdin)
process.stdin.on('end', () =>
  main(inputString.trim().split('\n').map(s => s.trim())))

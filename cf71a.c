/* https://codeforces.com/problemset/problem/71/A */
#include <stdio.h>
#include <string.h>

void
answer(char word[101], int len)
{
	if(len <= 10)
		puts(word);
	else
		printf("%c%d%c\n", word[0], len - 2, word[len - 1]);
}

int
main(void)
{
	int i;
	char word[101];

	scanf("%d", &i);

	while(i--) {
		scanf("%s", word);
		answer(word, strlen(word));
	}

	return 0;
}

((c-mode . ((c-file-style . "linux")
	    (whitespace-style . (face tabs spaces trailing lines space-before-tab indentation empty space-after-tab space-mark tab-mark missing-newline-at-eof))
	    (eval . (whitespace-mode)))))

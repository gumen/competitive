// https://codeforces.com/problemset/problem/263/A

function main(stdin) {
  const matrix = stdin.map(row => row.split(' ').map(Number))
  let y = 0; let x = 0

  while(matrix[y][x] !== 1)
    if(x < 5) { x++ }
    else { x = 0; y++ }

  process.stdout.write(Math.abs(y - 2) + Math.abs(x - 2) + '')
}

// Handle standard input
process.stdin.resume()
process.stdin.setEncoding('utf-8')

let inputString = ''

process.stdin.on('data', (inputStdin) => inputString += inputStdin)
process.stdin.on('end', () =>
  main(inputString.trim().split('\n').map(s => s.trim())))

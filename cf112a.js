// https://codeforces.com/problemset/problem/112/A

function main(stdin) {
  const a = stdin.shift().toLowerCase()
  const b = stdin.shift().toLowerCase()
  process.stdout.write(a.localeCompare(b) + '')
}

// Handle standard input
process.stdin.resume()
process.stdin.setEncoding('utf-8')

let inputString = ''

process.stdin.on('data', (inputStdin) => inputString += inputStdin)
process.stdin.on('end', () =>
  main(inputString.trim().split('\n').map(s => s.trim())))

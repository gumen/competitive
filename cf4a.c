/* https://codeforces.com/problemset/problem/4/A */
#include <stdio.h>

int
main(void)
{
	int weight;
	char *result = "NO";

	scanf("%d", &weight);

	if(weight != 2 && weight % 2 == 0)
		result = "YES";

	printf("%s", result);
	return 0;
}

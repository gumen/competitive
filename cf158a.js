// https://codeforces.com/problemset/problem/158/A

function main(stdin) {
  const [n, k] = stdin.shift().split(' ').map(Number)
  const as     = stdin.shift().split(' ').map(Number)
  process.stdout.write(as.filter(a => a > 0 && a >= as[k-1]).length + '')
}

// Handle standard input
process.stdin.resume()
process.stdin.setEncoding('utf-8')

let inputString = ''

process.stdin.on('data', (inputStdin) => inputString += inputStdin)
process.stdin.on('end', () =>
  main(inputString.trim().split('\n').map(s => s.trim())))

/* https://codeforces.com/problemset/problem/231/A */
#include <stdio.h>

int
main(void)
{
	int tasks_count, t[3], result = 0;

	scanf("%d", &tasks_count);

	while(tasks_count--) {
		scanf("%d %d %d", &t[0], &t[1], &t[2]);
		if(t[0] + t[1] + t[2] >= 2) result++;
	}

	printf("%d", result);
	return 0;
}

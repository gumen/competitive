/* https://codeforces.com/problemset/problem/158/A */
#include <stdio.h>

int
main(void)
{
	int n, k, s[51], i, result = 0;

	scanf("%d %d", &n, &k);
	k -= 1;

	for(i = 0; i < n; i++) scanf("%d", &s[i]);
	for(i = 0; i < n; i++) if(s[i] > 0 && s[i] >= s[k]) result++;

	printf("%d", result);
	return 0;
}

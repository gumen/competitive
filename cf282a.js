// https://codeforces.com/problemset/problem/282/A

function main(stdin) {
  stdin.shift()
  const result = stdin.reduce((acc, x) => {
    if(x.includes('++')) return acc + 1
    if(x.includes('--')) return acc - 1
  }, 0)
  process.stdout.write(result + '')
}

// Handle standard input
process.stdin.resume()
process.stdin.setEncoding('utf-8')

let inputString = ''

process.stdin.on('data', (inputStdin) => inputString += inputStdin)
process.stdin.on('end', () =>
  main(inputString.trim().split('\n').map(s => s.trim())))

// https://codeforces.com/problemset/problem/4/A

function main(weight) {
  process.stdout.write(weight > 2 && weight % 2 === 0 ? 'YES' : 'NO')
}

// Handle standard input
process.stdin.resume()
process.stdin.setEncoding('utf-8')

let inputString = ''

process.stdin.on('data', (inputStdin) => inputString += inputStdin)
process.stdin.on('end', () => main(+inputString.trim()))

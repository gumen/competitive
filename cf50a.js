// https://codeforces.com/problemset/problem/50/A

function main(stdin) {
  const [m, n] = stdin[0].split(' ').map(Number)
  process.stdout.write(Math.floor(m * n / 2) + '')
}

// Handle standard input
process.stdin.resume()
process.stdin.setEncoding('utf-8')

let inputString = ''

process.stdin.on('data', (inputStdin) => inputString += inputStdin)
process.stdin.on('end', () =>
  main(inputString.trim().split('\n').map(s => s.trim())))

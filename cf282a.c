/* https://codeforces.com/problemset/problem/282/A */
#include <stdio.h>

int
main(void)
{
	int i, result = 0;
	char operation[3];

	scanf("%d", &i);
	while(i--) {
		scanf("%s", operation);

		if(operation[1] == '+')
			result++;
		else
			result--;
	}

	printf("%d", result);
	return 0;
}

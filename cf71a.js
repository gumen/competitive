// https://codeforces.com/problemset/problem/71/A

function main(stdin) {
  for(let i = 1; i < stdin.length; i++) { // start from 1 to skip first input
    const w = stdin[i] // word
    const l = w.length // word length
    process.stdout.write((l > 10 ? `${w[0]}${l-2}${w[l-1]}` : w) + '\n')
  }
}

// Handle standard input
process.stdin.resume()
process.stdin.setEncoding('utf-8')

let inputString = ''

process.stdin.on('data', (inputStdin) => inputString += inputStdin)
process.stdin.on('end', () =>
  main(inputString.trim().split('\n').map(s => s.trim())))

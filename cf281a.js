// https://codeforces.com/problemset/problem/281/A

function main(stdin) {
  process.stdout.write(stdin[0].charAt(0).toUpperCase() + stdin[0].slice(1))
}

// Handle standard input
process.stdin.resume()
process.stdin.setEncoding('utf-8')

let inputString = ''

process.stdin.on('data', (inputStdin) => inputString += inputStdin)
process.stdin.on('end', () =>
  main(inputString.trim().split('\n').map(s => s.trim())))
